<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## About Laravel

This code is based on Laravel 5.5

## How to run the demo
- run composer install like:
  composer install
- run php artisan command like: 
  php artisan recipe:finder fridge.csv recipes.json
- run phpunit test like:
./vendor/phpunit/phpunit/phpunit tests/Unit/RecipeFinderTest.php 

## source files (the code where I wrote)
- app/Console/Commands/RecipeFinder.php
- app/Services/RecipeFinder.php
- tests/Unit/RecipeFinderTest.php

## the csv and json files
these filess are under storage/app

## notes
The unit test is pretty simple, no useful test at all,
basically for the completeness of the description.

It's impossible to find the saladsandwich recipe if 
the example fridge.csv has mixed salad,500,grams,26/12/2016
as it's already expired.

"item" : "mixedsalad"  in the recipes.json should be
mixed salad