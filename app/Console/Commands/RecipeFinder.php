<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Storage;

class RecipeFinder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'recipe:finder {csv : The input csv file} {json : The input json file}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $csvFile = $this->argument('csv');
        $jsonFile = $this->argument('json');

        $service = new \App\Services\RecipeFinder();
        $recipes = $service->findBestRecipe($csvFile, $jsonFile);

        $count = $recipes->count();
        if ($count > 0) {
            $first = $recipes->first();
            $this->info($first['name']);
        } else {
            $this->info('Order Takeout');
        }
    }

}
