<?php

namespace App\Services;

class RecipeFinder
{
    public function finderRecipes($validItems, $jsonArr)
    {
        $recipes = collect();
        $validItemsAmount = $validItems->pluck('amount', 'item')->all();
        foreach ($jsonArr as $key => $recipe) {
            //find the closet ingredient
            $found = true;
            $ingredients = collect();
            foreach ($recipe->ingredients as $in) {
                if (array_key_exists($in->item , $validItemsAmount) && $in->amount <= $validItemsAmount[$in->item]) {
                    foreach ($validItems as $item) {
                        if ($in->item == $item['item']) {
                            $ingredients->push($item);
                        }
                    }
                    continue;
                } else {
                    $found = false;
                    break;
                }
            }
            if ($found) {
                $ingredientsSort = $ingredients->sortBy(function($item) {
                    $time = strtotime(str_replace('/', '-', $item['useby']));
                    return $time;
                });
                $first = $ingredientsSort->first();
                $recipes->push(['name' => $recipe->name, 'useby' => $first['useby']]);
            }
        }
        $recipesSort = $recipes->sortBy(function($item) {
            $time = strtotime(str_replace('/', '-', $item['useby']));
            return $time;
        });

        return $recipesSort;
    }

    public function removeInvalidRecipes($jsonArr, $expiredItems)
    {
        foreach ($jsonArr as $key => $recipe) {
            $hasExpired = false;
            //get the expired out
            foreach ($recipe->ingredients as $in) {
                foreach ($expiredItems as $expired) {
                    if ($in->item == $expired['item']) {
                        $hasExpired = true;
                        break;
                    }
                }
                if ($hasExpired) {
                    break;
                }
            }

            if ($hasExpired) {
                //remove from recipes
                unset($jsonArr[$key]);
            }
        }

        return $jsonArr;
    }

    public function csvToArray($filename = '', $header, $delimiter = ',')
    {
        if (!file_exists($filename) || !is_readable($filename))
            return false;

        $data = array();
        if (($handle = fopen($filename, 'r')) !== false)
        {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false)
            {
                $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }

        return $data;
    }

    /**
     * @param $csvArr
     * @return static
     */
    public function getExpiredItems($csvArr)
    {
        $expiredItems = collect($csvArr)->filter(function ($value, $key) {
            $today = strtotime('12:00:00');
            $expire = strtotime(str_replace('/', '-', $value['useby']));
            if ($expire < $today) {
                return true;
            }
        });
        return $expiredItems;
    }

    /**
     * @param $csvArr
     * @return static
     */
    public function getValidItems($csvArr)
    {
        $validItems = collect($csvArr)->filter(function ($value, $key) {
            $today = strtotime('12:00:00');
            $expire = strtotime(str_replace('/', '-', $value['useby']));
            if ($expire >= $today) {
                return true;
            }
        });

        $validItems = $validItems->sortByDesc(function ($item) {
            $time = strtotime(str_replace('/', '-', $item['useby']));
            return $time;
        });
        return $validItems;
    }

    /**
     * @param $csvFile
     * @param $jsonFile
     * @return static
     */
    public function findBestRecipe($csvFile, $jsonFile)
    {
        $header = ['item', 'amount', 'unit', 'useby'];
        $csvArr = $this->csvToArray(storage_path() . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . $csvFile, $header);

        $expiredItems = $this->getExpiredItems($csvArr);

        $validItems = $this->getValidItems($csvArr);

        $jsonContent = file_get_contents(storage_path() . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . $jsonFile);
        $jsonArr = json_decode($jsonContent);

        $jsonArr = $this->removeInvalidRecipes($jsonArr, $expiredItems);

        $recipes = $this->finderRecipes($validItems, $jsonArr);
        return $recipes;
    }
}